package com.skyword.pushpublish.integration

import org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder
import org.junit.platform.launcher.core.LauncherFactory
import org.junit.platform.launcher.listeners.SummaryGeneratingListener
import java.io.PrintWriter

fun main(args: Array<String>) {
    try {
        System.setProperty("baseUrl", trimParam(args[0]))
        System.setProperty("clientId", trimParam(args[1]))
        System.setProperty("clientSecret", trimParam(args[2]))

        println("baseUrl = " + System.getProperty("baseUrl"))
        println("clientId = " + System.getProperty("clientId"))
        println("clientSecret = " + System.getProperty("clientSecret"))

        val request = LauncherDiscoveryRequestBuilder.request()
            .selectors(
                selectPackage("com.skyword.pushpublish.integration")
            )
            .build()

        val launcher = LauncherFactory.create()

        val listener = SummaryGeneratingListener();
        launcher.registerTestExecutionListeners(listener)
        launcher.execute(request)

        val pw = PrintWriter(System.out)
        listener.summary.printTo(pw)
        listener.summary.printFailuresTo(pw)
        pw.close()
    } catch (e: Exception) {
        println(e.message)
    }
}

fun trimParam(_s: String): String {
    return if (_s.contains('='))
        _s.substring(_s.indexOf('=') + 1)
    else
        _s
}
