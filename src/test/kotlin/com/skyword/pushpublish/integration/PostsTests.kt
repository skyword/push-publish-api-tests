package com.skyword.pushpublish.integration

import com.github.javafaker.Faker
import com.skyword.pushpublish.integration.body.*
import io.restassured.RestAssured
import io.restassured.RestAssured.get
import io.restassured.response.Response
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.net.URI
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeUnit

class PostsTests : AbstractApiEndpointTest() {


    private lateinit var postBody: PostBody
    private lateinit var postPosts: Response

    private lateinit var getPostLink: String
    private lateinit var getPostId: String
    private lateinit var post: PostBody

    @BeforeAll
    internal fun createNewPost() {
        val contentType = contentTypes.maxBy { it.fields.size }!!
        val author = authors.shuffled().first()
        postBody = PostGenerator().generate(contentType, author)
        postPosts = RestAssured.given(postRequestSpec)
            .body(postBody)
            .post("/posts")

        getPostLink = postPosts.getHeader("Link")
        getPostId = getIdFromLink(getPostLink, "/posts/")

        val getPost = get("posts/$getPostId")
        post = getPost.then().extract().`as`(PostBody::class.java)
    }

    @Test
    fun `POST posts returns 201`() {
        postPosts.then().statusCode(201)
    }

    @Test
    fun `POST posts returns Link header`() {
        assertThat(getPostLink).contains("skyword/v1/posts")
        val url = URI(getPostLink)
        assertThat(url).hasNoParameters().hasNoQuery().hasNoFragment().hasNoUserInfo()
    }

    @Test
    fun `GET posts returns new post data`() {
        val softly = SoftAssertions()
        softly.assertThat(post.id).isNotEmpty.isEqualTo(getPostId)
        softly.assertThat(post.url).isNotEmpty
        softly.assertThat(post.created).isNotNull
        softly.assertThat(post.updated).isNotNull
        softly.assertAll()
    }

    @Test
    fun `GET posts returns sent post data`() {
        val softly = SoftAssertions()
        softly.assertThat(post.skywordId).isEqualTo(postBody.skywordId)
        softly.assertThat(post.author).isEqualTo(postBody.author)
        post.fields.forEach { returnedField ->
            val sentField = postBody.fields.find { it.name == returnedField.id } ?: return
            softly.assertThat(returnedField.value).isEqualTo(sentField.value)
        }
        softly.assertAll()
    }
}


class PostGenerator {
    private val faker = Faker()

    fun generate(
        contentType: ContentTypeBody,
        author: AuthorsResponseBody
    ): PostBody {
        val skywordId = ThreadLocalRandom.current().nextLong()

        return PostBody(
            skywordId = skywordId,
            type = contentType.id,
            title = faker.book().title(),
            author = author.id,
            trackingTag = generateTrackingTag(skywordId),
            fields = generateFields(contentType.fields)
        )
    }

    private fun generateTrackingTag(skywordId: Long): String {
        val src = "//tracking.skyword.com:7090/tracker.js?contentId=$skywordId"
        return "<script async type='text/javascript' src='$src'></script>"
    }

    private fun generateFields(fields: List<ContentTypeFieldBody>): MutableList<Field> {
        return fields.filter { it.type != "TITLE" }.mapNotNull { field(it) }.toMutableList()
    }

    private fun field(contentTypeFieldBody: ContentTypeFieldBody): Field? {
        val name = contentTypeFieldBody.name
        return when (contentTypeFieldBody.type) {
            "TEXT-FIELD" -> Field(name = name, value = faker.lorem().sentence(), type = FieldType.TEXT.name)
            "TEXT-AREA" -> Field(name = name, value = faker.lorem().paragraph(), type = FieldType.TEXT.name)
            "SELECT-SINGLE" -> Field(name = name, value = faker.lorem().word(), type = FieldType.TAXONOMY.name)
            "SELECT-MULTIPLE" -> Field(
                name = name,
                value = faker.lorem().words().joinToString(","),
                type = FieldType.TAXONOMY.name
            )
            "TAXONOMY-SELECT-SINGLE" -> Field(name = name, value = "1", type = FieldType.TAXONOMY.name)
            "TAXONOMY-SELECT-MULTIPLE" -> Field(name = name, value = "1,2,3", type = FieldType.TAXONOMY.name)
            "TAXONOMY-TEXT-FIELD" -> Field(name = name, value = "1,2,3", type = FieldType.TAXONOMY.name)
            "CHECKBOX" -> Field(name = name, value = "true", type = FieldType.BOOLEAN.name)
            "SUMMARY" -> Field(name = name, value = faker.lorem().paragraph(), type = FieldType.TEXT.name)
            "IMAGE" -> Field(name = name, value = "1", type = FieldType.IMAGE.name)
            "DATETIME" -> Field(
                name = name,
                value = faker.date().past(39, TimeUnit.DAYS).toISO8601(),
                type = FieldType.DATE.name
            )
            "UNKNOWN" -> null
            else -> null
        }
    }
}

enum class FieldType {
    IMAGE,
    TAXONOMY,
    TEXT,
    HTML,
    DATE,
    BOOLEAN,
    META
}

fun Date.toISO8601(): String {
    val tz = TimeZone.getTimeZone("UTC")
    val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
    df.timeZone = tz
    return df.format(this)
}