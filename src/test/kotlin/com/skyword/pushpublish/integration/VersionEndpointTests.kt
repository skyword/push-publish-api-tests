package com.skyword.pushpublish.integration

import com.skyword.pushpublish.integration.body.VersionBody
import io.restassured.RestAssured
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test

class VersionEndpointTests : AbstractApiEndpointTest() {

    @Test
    fun `GET version returns 200 and all values`() {
        val version = RestAssured.get("/version")
            .then()
            .spec(okResponseSpec)
            .extract().`as`(VersionBody::class.java)

        val softly = SoftAssertions()

        softly.assertThat(version.plugin.version).isNotEmpty().isExactlyInstanceOf(String::class.java)
        softly.assertThat(version.plugin.language.name).isNotEmpty().isExactlyInstanceOf(String::class.java)
        softly.assertThat(version.plugin.language.version).isNotEmpty().isExactlyInstanceOf(String::class.java)
        softly.assertThat(version.cms.name).isNotEmpty().isExactlyInstanceOf(String::class.java)
        softly.assertThat(version.cms.version).isNotEmpty().isExactlyInstanceOf(String::class.java)

        softly.assertAll()
    }
}