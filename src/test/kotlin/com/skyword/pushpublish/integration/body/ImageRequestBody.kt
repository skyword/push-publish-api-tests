package com.skyword.pushpublish.integration.body

data class ImageRequestBody(
    val filename: String,
    val filesize: Long? = 0,
    val file: String
)