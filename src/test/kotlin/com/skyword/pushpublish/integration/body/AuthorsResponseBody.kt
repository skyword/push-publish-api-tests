package com.skyword.pushpublish.integration.body

data class AuthorsResponseBody(
    val id: String,
    val firstName: String,
    val lastName: String,
    val email: String,
    val byline: String,
    val icon: String? = null
)
