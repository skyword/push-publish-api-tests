package com.skyword.pushpublish.integration.body

import java.time.OffsetDateTime

data class PostBody(
    val id: String? = null,
    val skywordId: Long,
    val type: String,
    val title: String,
    val created: OffsetDateTime? = null,
    val updated: OffsetDateTime? = null,
    val url: String? = null,
    val author: String,
    val trackingTag: String? = null,
    val fields: MutableList<Field>
)

data class Field(
    val id: String? = null,
    val name: String,
    val value: String,
    val type: String
)

enum class FieldType {
    IMAGE,
    TAXONOMY,
    TEXT,
    HTML,
    DATE,
    BOOLEAN,
    META
}