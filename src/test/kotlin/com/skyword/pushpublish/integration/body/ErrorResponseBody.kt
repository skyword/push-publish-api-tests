package com.skyword.pushpublish.integration.body

data class ErrorResponseBody(
    var message: String,
    var description: String
)
