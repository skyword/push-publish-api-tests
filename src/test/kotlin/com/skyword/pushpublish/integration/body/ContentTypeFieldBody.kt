package com.skyword.pushpublish.integration.body

data class ContentTypeFieldBody(
    val id: String,
    val name: String,
    val label: String? = null,
    val description: String? = null,
    val required: Boolean? = false,
    val type: String
)