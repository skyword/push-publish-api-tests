package com.skyword.pushpublish.integration.body

data class TaxonomyBody(

    val id: String,
    val name: String,
    val description: String,
    val numTerms: Int

)