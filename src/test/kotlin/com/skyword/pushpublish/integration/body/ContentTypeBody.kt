package com.skyword.pushpublish.integration.body

data class ContentTypeBody(
    val id: String,
    val name: String,
    val description: String? = null,
    val fields: List<ContentTypeFieldBody> = emptyList()
)