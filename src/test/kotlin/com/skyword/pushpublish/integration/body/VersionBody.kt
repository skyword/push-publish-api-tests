package com.skyword.pushpublish.integration.body

data class VersionBody(
    val plugin: Plugin,
    val cms: Cms
)

data class Plugin(
    val version: String,
    val language: Language
)

data class Language(
    val name: String,
    val version: String
)

data class Cms(
    val name: String,
    val version: String
)
