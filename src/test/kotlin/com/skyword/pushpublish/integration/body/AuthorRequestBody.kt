package com.skyword.pushpublish.integration.body


data class AuthorRequestBody(
    var email: String,
    var firstName: String?,
    var lastName: String?,
    var byline: String?
)