package com.skyword.pushpublish.integration.body


data class ImageResponseBody(
    val id: String,
    val type: String? = null,
    val url: String,
    val metadata: ImageMetadataBody? = null
)