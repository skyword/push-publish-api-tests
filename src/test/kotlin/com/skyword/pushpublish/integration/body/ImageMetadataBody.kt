package com.skyword.pushpublish.integration.body


data class ImageMetadataBody(
    val title: String?,
    val alt: String?
)