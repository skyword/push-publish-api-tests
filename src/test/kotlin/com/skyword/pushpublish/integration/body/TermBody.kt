package com.skyword.pushpublish.integration.body


data class TermBody(
    val id: String? = null,
    val value: String,
    val parent: String? = null
)