package com.skyword.pushpublish.integration.body


data class TokenBody(
    var grant_type: String,
    var scope: String,
    var client_id: String,
    var client_secret: String
)