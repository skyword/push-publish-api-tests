package com.skyword.pushpublish.integration

import com.skyword.pushpublish.integration.body.AuthorRequestBody
import com.skyword.pushpublish.integration.body.AuthorsResponseBody
import com.skyword.pushpublish.integration.body.ErrorResponseBody
import io.restassured.RestAssured
import io.restassured.RestAssured.get
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test

class AuthorsTests : AbstractApiEndpointTest() {

    private val firstAuthorId by lazy {authors[0].id }
    private val getSingleAuthor by lazy  {get("authors/$firstAuthorId")}

    @Test
    fun `GET returns 200`() {
        getAuthors.then().spec(okResponseSpec)
    }

    @Test
    fun `GET returns a list of authors`() {
        assertThat(authors).isNotEmpty()
    }

    @Test
    fun `GET authors items are formatted correctly`() {
        val author = authors.first()

        val softly = SoftAssertions()
        authorBodyFormattedCorrectly(softly, author)
        softly.assertAll()
    }

    @Test
    fun `GET returns the required paginated headers`() {
        val softly = SoftAssertions()
        assertCorrectPaginationHeaders(softly, getAuthors)
        softly.assertAll()
    }

    @Test
    fun `GET single author returns 200`() {
        getSingleAuthor.then()
            .spec(okResponseSpec)
    }

    @Test
    fun `GET single author returns one author and is formatted correctly`() {
        val author = getSingleAuthor.then()
            .extract().`as`(AuthorsResponseBody::class.java)

        val softly = SoftAssertions()

        softly.assertThat(author).isNotNull()
        authorBodyFormattedCorrectly(softly, author)
        softly.assertAll()
    }

    @Test
    fun `POST create author`() {
        val body = RestAssured.given(postRequestSpec)
            .body(AuthorRequestBody("mike@test.com", "Mike", "Test", "Testing a bunch of calls."))
            .post("/authors")
            .then()
            .statusCode(201)
            .extract().asString()


        if (body == "null") {
            assertThat(body).contains("null")
        } else {
            assertThat(body).isNullOrEmpty()
        }
    }

    @Test
    fun `POST create duplicate author`() {
        val body = RestAssured.given(postRequestSpec)
            .body(AuthorRequestBody("tester1@test.com", "Stevesx", "Testedrer", "Testing a bunch of testscsss."))
            .post("/authors")
            .then()
            .statusCode(422)
            .extract().`as`(ErrorResponseBody::class.java)

        assertThat(body).hasFieldOrPropertyWithValue("message", "Author already exists")

    }

    private fun authorBodyFormattedCorrectly(softly: SoftAssertions, author: AuthorsResponseBody) {
        softly.assertThat(author.id).isNotEmpty().isExactlyInstanceOf(String::class.java)
        softly.assertThat(author.firstName).isNotEmpty().isExactlyInstanceOf(String::class.java)
        softly.assertThat(author.lastName).isNotEmpty().isExactlyInstanceOf(String::class.java)
        softly.assertThat(author.email).isNotEmpty().isExactlyInstanceOf(String::class.java)
        softly.assertThat(author.byline).isNotEmpty().isExactlyInstanceOf(String::class.java)
        if (author.icon != null) {
            softly.assertThat(author.icon).isExactlyInstanceOf(String::class.java)
        }
    }

}