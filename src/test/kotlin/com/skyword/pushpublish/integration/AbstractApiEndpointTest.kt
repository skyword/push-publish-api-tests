package com.skyword.pushpublish.integration

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.skyword.pushpublish.integration.body.AuthorsResponseBody
import com.skyword.pushpublish.integration.body.ContentTypeBody
import com.skyword.pushpublish.integration.body.TaxonomyBody
import com.skyword.pushpublish.integration.body.Token
import io.restassured.RestAssured
import io.restassured.RestAssured.*
import io.restassured.builder.RequestSpecBuilder
import io.restassured.builder.ResponseSpecBuilder
import io.restassured.config.ObjectMapperConfig
import io.restassured.config.RestAssuredConfig
import io.restassured.filter.log.ErrorLoggingFilter
import io.restassured.filter.log.LogDetail
import io.restassured.filter.log.RequestLoggingFilter
import io.restassured.filter.log.ResponseLoggingFilter
import io.restassured.response.Response
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class AbstractApiEndpointTest {

    lateinit var accessToken: String

    protected val getRequestSpec by lazy {
        RequestSpecBuilder()
            .setAuth(oauth2(accessToken))
            .addQueryParam("XDEBUG_SESSION_START", "PHPSTORM")
            //Uncomment to log full request to system out
            .addFilter(RequestLoggingFilter(LogDetail.ALL))
            //Uncomment to log full response to system out
            .addFilter(ResponseLoggingFilter(LogDetail.ALL))
            //Uncomment to log errors to system out
            .addFilter(ErrorLoggingFilter())
            .build()!!
    }

    @BeforeAll
    fun setup() {

        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(ObjectMapperConfig().jackson2ObjectMapperFactory { _, _ ->
             ObjectMapper().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                 .registerModule(JavaTimeModule())
                 .registerModule(KotlinModule())
                 .setSerializationInclusion(JsonInclude.Include.NON_NULL)
        })
//        RestAssured.baseURI = System.getProperty("baseUrl")
//                ?: throw Exception("Please pass in base url to test via -DbaseUrl={BASEURL} on the command line")
//        val clientId = System.getProperty("clientId")
//                ?: throw Exception("Please pass in client id to test via -DclientId={CLIENTID} on the command line")
//        val clientSecret = System.getProperty("clientSecret")
//                ?: throw Exception("Please pass in client secret to test via -DclientSecret={CLIENTSECRET} on the command lineRestAssured.baseURI = "http://drupal8.dev.local/skyword/v1"
        RestAssured.baseURI = "http://drupal8.dev.local/skyword/v1"
        val clientId= "824c2882-bb55-4a65-91a1-3b7ee8ff8ceb"
        val clientSecret = "secret"

        val tokenRequest = given()
            .multiPart("client_id", clientId)
            .multiPart("client_secret", clientSecret)
            .multiPart("grant_type", "client_credentials")
            .multiPart("scope", "skyword")

        accessToken = tokenRequest.`when`()
            .post("/oauth2/token")
            .then()
            .extract().`as`(Token::class.java)
            .accessToken

        //RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
        RestAssured.requestSpecification = getRequestSpec
    }

    protected val postRequestSpec by lazy {
        RequestSpecBuilder()
            .setAuth(oauth2(accessToken))
            .setContentType("application/json")
            .addQueryParam("XDEBUG_SESSION_START", "PHPSTORM")
            .build()
    }

    protected val okResponseSpec = ResponseSpecBuilder()
        .expectStatusCode(200)
        .build()!!

    protected val getTaxonomyList by lazy { get("taxonomies")!! }
    protected val taxonomies by lazy { getBodyList<TaxonomyBody>(getTaxonomyList) }

    protected val getContentTypes by lazy { get("content-types")!! }
    protected val contentTypes by lazy { getBodyList<ContentTypeBody>(getContentTypes) }

    protected val getAuthors by lazy { get("authors")!! }
    protected val authors by lazy { getBodyList<AuthorsResponseBody>(getAuthors) }

    protected fun assertCorrectPaginationHeaders(softly: SoftAssertions, response: Response) {
        val totalCountHeader = response.getHeader("X-Total-Count")
        val linkHeader = response.getHeader("Link")

        softly.assertThat(totalCountHeader).isNotEmpty().containsOnlyDigits()
        softly.assertThat(linkHeader).isNotEmpty().contains("rel=")//todo add uri regex
    }

    inline fun <reified T> getBodyList(response: Response): List<T> {
        return response.body.jsonPath().getList<T>("", T::class.java)
    }

    protected fun getIdFromLink(link: String, delimiter: String): String {
        return link.split(delimiter)[1]
    }
}