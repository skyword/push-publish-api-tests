package com.skyword.pushpublish.integration

import com.skyword.pushpublish.integration.body.ContentTypeBody
import io.restassured.RestAssured.get
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test


class ContentTypesTests : AbstractApiEndpointTest() {

    private val types = listOf(
        "TITLE",
        "TEXT-FIELD",
        "TEXT-AREA",
        "SELECT-SINGLE",
        "SELECT-MULTIPLE",
        "TAXONOMY-SELECT-SINGLE",
        "TAXONOMY-SELECT-MULTIPLE",
        "TAXONOMY-TEXT-FIELD",
        "CHECKBOX",
        "SUMMARY",
        "IMAGE",
        "DATETIME",
        "UNKNOWN"
    )

    private val firstContentTypeId by lazy { contentTypes[0].id }
    private val getSingleContentType  by lazy { get("content-types/$firstContentTypeId") }

    @Test
    fun `GET returns 200`() {
        getContentTypes.then().spec(okResponseSpec)
    }

    @Test
    fun `GET returns a list of content types`() {
        assertThat(contentTypes).isNotEmpty

    }

    @Test
    fun `GET content-types items are formatted correctly`() {
        val contentType = contentTypes.first()

        val softly = SoftAssertions()
        contentTypesBodyFormattedCorrectly(softly, contentType)
        softly.assertAll()
    }

    @Test
    fun `GET returns the required paginated headers`() {
        val softly = SoftAssertions()
        assertCorrectPaginationHeaders(softly, getContentTypes)
    }

    @Test
    fun `GET single content-type returns 200`() {
        getSingleContentType.then().spec(okResponseSpec)
    }

    @Test
    fun `GET single content-tyoe returns one content-type and is formatted correctly`() {
        val contentType = getSingleContentType.then().extract().`as`(ContentTypeBody::class.java)

        assertThat(contentType).isNotNull

        val softly = SoftAssertions()
        contentTypesBodyFormattedCorrectly(softly, contentType)
        softly.assertAll()
    }

    private fun contentTypesBodyFormattedCorrectly(softly: SoftAssertions, contentType: ContentTypeBody) {
        softly.assertThat(contentType.id).isNotEmpty
        softly.assertThat(contentType.name).isNotEmpty
        softly.assertThat(contentType.fields).isNotEmpty

        contentType.fields.forEach { field ->
            softly.assertThat(field.id).isNotEmpty
            softly.assertThat(field.name).isNotEmpty
            softly.assertThat(field.type).isUpperCase.isIn(types)
        }
    }
}
