package com.skyword.pushpublish.integration

import com.skyword.pushpublish.integration.body.TaxonomyBody
import com.skyword.pushpublish.integration.body.TermBody
import io.restassured.RestAssured
import io.restassured.RestAssured.get
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test

class TaxonomiesTests : AbstractApiEndpointTest() {


    private val firstTaxonomyId by lazy { taxonomies[0].id }
    private val getSingleTaxonomy by lazy { get("taxonomies/$firstTaxonomyId") }

    private val getTermsList by lazy { get("taxonomies/$firstTaxonomyId/terms") }

    @Test
    fun `GET taxonomy list returns 200`() {
        getTaxonomyList.then().spec(okResponseSpec)
    }

    @Test
    fun `GET terms list returns 200`() {
        getTermsList.then().spec(okResponseSpec)
    }

    @Test
    fun `GET taxonomy list returns a list of taxonomies`() {
        assertThat(taxonomies).isNotEmpty()
    }

    @Test
    fun `GET taxonomy list returns the required paginated headers`() {
        val softly = SoftAssertions()
        assertCorrectPaginationHeaders(softly, getTaxonomyList)
        softly.assertAll()
    }

    @Test
    fun `GET single taxonomy returns 200`() {
        getSingleTaxonomy.then().spec(okResponseSpec)
    }

    @Test
    fun `GET taxonomy items are formatted correctly`() {
        val taxonomyList = getBodyList<TaxonomyBody>(getTaxonomyList)
        val taxonomyBody = taxonomyList.first()

        val softly = SoftAssertions()
        taxonomyBodyFormattedCorrectly(softly, taxonomyBody)
        softly.assertAll()
    }

    @Test
    fun `GET terms items are formatted correctly`() {
        val termsList = getBodyList<TermBody>(getTermsList)
        val termsBody = termsList.first()

        val softly = SoftAssertions()
        termsBodyFormattedCorrectly(softly, termsBody)
        softly.assertAll()
    }

    @Test
    fun `GET terms list returns a list of terms`() {
        val termsList = getBodyList<TermBody>(getTermsList)
        assertThat(termsList).isNotEmpty()
    }

    @Test
    fun `GET terms list returns the required paginated headers`() {
        val softly = SoftAssertions()
        assertCorrectPaginationHeaders(softly, getTermsList)
        softly.assertAll()
    }

    @Test
    fun `GET terms returns proper number of terms`() {
        val taxonomy = getSingleTaxonomy.then().extract().`as`(TaxonomyBody::class.java)
        val totalTerms = getTermsList.header("X-Total-Count").toInt()

        assertThat(taxonomy.numTerms).isEqualTo(totalTerms)
    }

    private fun taxonomyBodyFormattedCorrectly(softly: SoftAssertions, taxonomy: TaxonomyBody) {
        softly.assertThat(taxonomy.id).isNotEmpty.isExactlyInstanceOf(String::class.java)
        softly.assertThat(taxonomy.name).isNotEmpty.isExactlyInstanceOf(String::class.java)
        softly.assertThat(taxonomy.numTerms).isNotZero.isExactlyInstanceOf(Integer::class.java)

        // TODO This field should be nullable, it is not required.
        softly.assertThat(taxonomy.description).isNotEmpty.isExactlyInstanceOf(String::class.java)
    }

    private fun termsBodyFormattedCorrectly(softly: SoftAssertions, term: TermBody) {
        softly.assertThat(term.id).isNotEmpty.isExactlyInstanceOf(String::class.java)
        softly.assertThat(term.value).isNotEmpty.isExactlyInstanceOf(String::class.java)
        if (term.parent != null) {
            softly.assertThat(term.parent).isNotEmpty.isExactlyInstanceOf(String::class.java)
        }
    }

    @Test
    fun `POST term`() {
        val body = RestAssured.given(postRequestSpec)
            .body(TermBody("First Test1", "planet_express"))
            .post("/taxonomies/$firstTaxonomyId/terms")
            .then()
            .statusCode(201)
            .extract().asString()

        if (body == "null") {
            assertThat(body).contains("null")
        } else {
            assertThat(body).isNullOrEmpty()
        }
    }

}